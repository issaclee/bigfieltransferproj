0.version: 0.5

1.준비사항 
  1.1. 자바가 1.6이상 설치되어 있어야 한다.
  1.2. ant 가 설치되어 있어야 한다.
  ================== 콘솔환경 ============================================== 
  1.3. 압축을 해제한다.
       BigFileTransferProj/resource/log4j.properties  파일을 다음과 같이 수정한다. 

       # 경로를 환경에 맞게 생성해 주세요.서버에 생성되는 로그파일이다.  
       log4j.appender.dailyfile.File = /usr/local/dev/nio/log/transfer.log

  1.4. BigFileTransferProj/resource/transfer.properties  파일을 다음과 같이 수정한다. 

        # transferServer.jar 가 구동되는 서버 아이피
		server.ip=192.168.0.54

		# transferServer.jar 가 구동되는 서버 포트 
		server.port=9026

		# send file folder..
		# 보낼 파일 폴더. transferClient.jar  모니터링할 폴더입니다. 환경에 맞게 수정해야 한다. 
		# you should modify path in your environment...

		send.file.folder=/Users/bonolee/dev/javanio/client/

		# recv file folder. transferServer.jar 가 write할 경로
		# you should modify path in your environment...
		# 서버로 저장될 폴더입니다. 환경에 맞게 수정해야 한다. 

		recv.file.folder=/usr/local/dev/nio/

		# recv file extention
		# 받은 파일의 확장자. 업무에 맞게 수정해야 한다. 
		recv.file.extention=.log

2.properties 파일 수정후에 ant로 패키징을 한다.
  $>ant
  성공하면 BigFileTransferProj/build 디렉토리가 생성되고 거기에 transferServer.jar 와 transferClient.jar 파일이 생성된다.
  
3.서버구동
	2.1. transferServer.jar  파일 위치로 이동한다.
	     $> java -jar transferServer.jar &


4.클라이언트 구동
    3.0. 해당 클라이언트는 shell로 만들어 cronjob에 등록한다. 
	3.1. transferClient.jar 파일 위치로 이동한다.
	     shell 에서 구동시킬 명령은 아래와 같다. 
	     $>java -jar transferClient.jar 
	     
================== eclipse환경 ==============================================
    1.3. BigFileTransferProj.zip 파일을 eclipse 를 통해  임포트하면 프로젝트가 생성된다.
         프로젝트 임포트하는 방법은 생략한다. 
         프로퍼티수정후에 build.xml에서 오른쪽 버튼을 누르고 Run AS>Ant build 를 선택하면 된다. 
 	     