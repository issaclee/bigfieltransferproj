package com.cg.network;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

import org.apache.log4j.Logger;

import com.cg.util.PropertiesManager;
import com.cg.util.Utils;

/**
 * 소켓서버 클래스입니다.
 * @author bonolee
 * 
 */
public class TransferToServer {
	private final Logger logger = Logger.getLogger(this.getClass());

	private ServerSocketChannel serverSocketChannel = null;

	private String recvFileFolder;

	private String serverIp;

	private int port;

	private int bufferSize;

	private String recvFileExtention;

	private int loopSkipCount;

	public TransferToServer() {
		this.serverIp = PropertiesManager.getInstance().get("server.ip");
		this.port = Integer.parseInt(PropertiesManager.getInstance().get("server.port"));
		this.bufferSize = Integer.parseInt(PropertiesManager.getInstance().get("buffer.capacity"));
		this.loopSkipCount = Integer.parseInt(PropertiesManager.getInstance().get("loop.skip.count"));
		this.recvFileFolder = PropertiesManager.getInstance().get("recv.file.folder");
		this.recvFileExtention = PropertiesManager.getInstance().get("recv.file.extention");
	}

	/**
	 * 소켓을 생성합니다.
	 * 
	 * @see
	 * @author crowdgeeks.co
	 * @date 2013. 10. 28.
	 */
	protected void initServer() {
		InetSocketAddress listenAddr = new InetSocketAddress(serverIp, port);

		try {
			serverSocketChannel = ServerSocketChannel.open();
			ServerSocket serverSocket = serverSocketChannel.socket();
			serverSocket.setReuseAddress(true);
			serverSocket.setReceiveBufferSize(bufferSize);
			serverSocket.setSoTimeout(30 * 1000);
			serverSocket.bind(listenAddr);

			logger.debug("Listening on port : " + listenAddr.toString());
		} catch (IOException e) {
			logger.debug("Failed to bind, is port : " + listenAddr.toString() + " already in use ? Error Msg : " + e.getMessage());
			logger.error(e);
		}

	}

	/**
	 * 채널에 도착한 데이터를 파일로 write합니다.
	 * 
	 * @see
	 * @author crowdgeeks.co
	 * @date 2013. 10. 28.
	 */
	private void readData() {
		ByteBuffer byteBuffer = ByteBuffer.allocate(bufferSize);
		FileOutputStream fileOutputStream = null;
		FileChannel fileChannel = null;
		long totalReceivedBytes = 0L;
		try {
			while (true) {
				SocketChannel socketChannel = serverSocketChannel.accept();
				logger.debug("Accepted : " + socketChannel);

				socketChannel.configureBlocking(true);
				socketChannel.socket().setTcpNoDelay(true);

				int nread = 0;
				StringBuilder fileFullNameBuilder = new StringBuilder();
				fileFullNameBuilder.append(recvFileFolder);
				fileFullNameBuilder.append(Utils.currentTimeToDateString());
				fileFullNameBuilder.append(recvFileExtention);

				logger.debug("create file[" + fileFullNameBuilder.toString() + "]");
				fileOutputStream = new FileOutputStream(fileFullNameBuilder.toString());
				fileChannel = fileOutputStream.getChannel();
				long startTime = System.currentTimeMillis();
				int loopCount = 0;
				fileFullNameBuilder.setLength(0);

				while ((nread = socketChannel.read(byteBuffer)) != -1) {
					try {
						// to read from bytebuffer
						byteBuffer.flip();

						totalReceivedBytes += nread;
						if((loopCount++) % loopSkipCount == 0) {
							logger.debug("loopCount[" + loopCount + "],nread[" + nread + "],totalReceivedBytes[" + totalReceivedBytes + "]");
						}

						//write to fileChannel 
						fileChannel.write(byteBuffer);
						byteBuffer.clear();

					} catch (IOException e) {
						logger.error(e);
						nread = -1;
					}
				}
				logger.debug("Total [" + (System.currentTimeMillis() - startTime) / 1000L + "]Seconds elapsed!!!");
			}
		} catch (IOException e) {
			logger.error(e);
		} finally {
			try {
				fileOutputStream.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
				fileChannel.close();
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	/**
	 * 메인 클래스 소켓서버를 기동합니다.
	 * @param args
	 * @see
	 * @author crowdgeeks.co
	 * @date 2013. 10. 28.
	 */
	public static void main(String[] args) {
		TransferToServer transferToServer = new TransferToServer();
		transferToServer.initServer();
		transferToServer.readData();
	}
}
