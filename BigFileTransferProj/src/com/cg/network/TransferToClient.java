package com.cg.network;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.cg.util.PropertiesManager;
import com.cg.util.Utils;

/**
 * 소켓 클라이언트 클래스입니다.
 * @author bonolee
 * 
 */
public class TransferToClient {

	private final Logger logger = Logger.getLogger(this.getClass());

	//	private String sendFileName;

	private String sendFileFolder;

	private long filechannelMaxSize;

	private String serverIp;

	private int port;

	private int bufferSize;

	public TransferToClient() {
		this.serverIp = PropertiesManager.getInstance().get("server.ip");
		this.port = Integer.parseInt(PropertiesManager.getInstance().get("server.port"));
		this.bufferSize = Integer.parseInt(PropertiesManager.getInstance().get("buffer.capacity"));
		//		this.sendFileName = PropertiesManager.getInstance().get("send.file.name");÷
		this.sendFileFolder = PropertiesManager.getInstance().get("send.file.folder");
		this.filechannelMaxSize = Long.parseLong(PropertiesManager.getInstance().get("filechannel.maxsize"));
	}

	public static void main(String[] args) throws IOException {
		TransferToClient transferToClient = new TransferToClient();
		//		sfc.sendFile();
		transferToClient.execute();
	}

	public void execute() {

		File folder = new File(sendFileFolder);
		if(folder.isDirectory()) {
			File[] files = folder.listFiles();
			List<File> fileList = new ArrayList<File>();

			for (File file : files) {
				long lastModifiedMiliseconds = file.lastModified();
				logger.debug("file:" + file.getName() + ",lastModified:" + lastModifiedMiliseconds);

				String nowDateString = Utils.timeMillisToDateString(System.currentTimeMillis());
				String targetDateString = Utils.timeMillisToDateString(lastModifiedMiliseconds);
				logger.debug("nowDateString:" + nowDateString + ",targetDateString:" + targetDateString);

				if(nowDateString.equals(targetDateString)) {
					fileList.add(file);
				}
			}

			sendFile(fileList);
		}
	}

	/**
	 * 서버로 파일을 전송합니다.
	 * 
	 * @see
	 * @author crowdgeeks.co
	 * @date 2013. 10. 28.
	 */
	public void sendFile(List<File> fileList) {

		FileChannel fileChannel = null;
		SocketChannel socketChannel = null;
		FileInputStream fileInputStream = null;

		for (File file : fileList) {
			try {
				SocketAddress socketAddress = new InetSocketAddress(serverIp, port);

				socketChannel = SocketChannel.open();
				socketChannel.socket().setTcpNoDelay(true);
				socketChannel.socket().setSendBufferSize(bufferSize);
				socketChannel.connect(socketAddress);
				// blockmode 
				socketChannel.configureBlocking(true);

				fileInputStream = new FileInputStream(file);
				fileChannel = fileInputStream.getChannel();

				long start = System.currentTimeMillis();

				long fileChannelSize = fileChannel.size();

				logger.debug(file.getName() + " SEND FILE TOTAL SIZE IS :" + fileChannelSize + " bytes");

				long totalBytes = 0L;

				if(filechannelMaxSize < fileChannelSize) {
					logger.debug("2Gbytes over process....");
					//ByteBuffer
					ByteBuffer byteBuffer = ByteBuffer.allocate(bufferSize);
					int readBytes = 0;

					while ((readBytes = fileChannel.read(byteBuffer)) != -1) {
						byteBuffer.flip();
						socketChannel.write(byteBuffer);

						totalBytes += readBytes;
						byteBuffer.clear();
					}
				} else {
					logger.debug("2Gbytes under process ....");
					//fileChannel.transferTo 2G 제한 
					totalBytes = fileChannel.transferTo(0, fileChannel.size(), socketChannel);
				}

				logger.debug("total bytes transferred--" + totalBytes + " and time taken in Seconds:" + (System.currentTimeMillis() - start) / 1000L);
			} catch (Exception e) {
				logger.error(e);
			} finally {
				if(fileChannel != null) {
					try {
						fileChannel.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						logger.error(e);
					}
				}
				if(fileInputStream != null) {
					try {
						fileInputStream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						logger.error(e);
					}
				}
				if(socketChannel != null) {
					try {
						socketChannel.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						logger.error(e);
					}
				}
			}
		}
	}
}
