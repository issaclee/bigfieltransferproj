package com.cg.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * properties 를 관리하는 클래스입니다.
 * @author bonolee
 * 
 */
public class PropertiesManager {

	private static PropertiesManager propertiesManager = new PropertiesManager();

	private Properties properties;

	//	private final String ENCODING_TYPE = "UTF-8";

	private final String PROPERTIES_FILE_PATH = "transfer.properties";

	private PropertiesManager() {
		init();
	}

	/**
	 * return single instance
	 * @return
	 * @see
	 * @author crowdgeeks.co
	 * @date 2013. 10. 29.
	 */
	public static PropertiesManager getInstance() {
		if(propertiesManager == null) {
			propertiesManager = new PropertiesManager();
		}

		return propertiesManager;
	}

	private void init() {

		this.properties = new Properties();

		try {
			// 한글이 깨지는 문제 때문에 인코딩을 지정해서 읽을수 있도록 함.
			//			this.properties.load(new BufferedReader(new InputStreamReader(new FileInputStream(PROPERTIES_FILE_PATH), ENCODING_TYPE)));
			this.properties.load(ClassLoader.getSystemResourceAsStream(PROPERTIES_FILE_PATH));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 키값에 해당하는 value를 리턴합니다. 없으면 null
	 * @param key
	 * @return
	 * @see
	 * @author crowdgeeks.co
	 * @date 2013. 10. 29.
	 */
	public String get(String key) {
		return properties.getProperty(key);
	}
}
