package com.cg.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * 유틸 클래스
 * @author bonolee
 * 
 */
public class Utils {
	public static final String DATE_FORMAT_YYYYMMDD = "yyyyMMdd";

	public static final String DATE_FORMAT_YYYYMMDDHHMMSS = "yyyyMMddHHmmss.SSS";

	public static void sleep(long miliseconds) {
		try {
			Thread.sleep(miliseconds);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * milliseconds 타임을 받아서 yyyymmdd 포맷의 날짜 문자열을 리턴합니다.
	 * @param timeMilliseconds
	 * @return
	 * @see
	 * @author crowdgeeks.co
	 * @date 2013. 10. 30.
	 */
	public static String timeMillisToDateString(long timeMilliseconds) {

		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTimeInMillis(timeMilliseconds);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_YYYYMMDD);

		return simpleDateFormat.format(calendar.getTime());
	}

	/**
	 * 현재시간을 yyyyMMddHHmmss형태의 문자열로 리턴합니다.
	 * @return
	 * @see
	 * @author crowdgeeks.co
	 * @date 2013. 10. 30.
	 */
	public static String currentTimeToDateString() {

		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_YYYYMMDDHHMMSS);

		return simpleDateFormat.format(calendar.getTime());
	}

	public static void main(String[] args) {
		System.out.println("now::" + timeMillisToDateString(System.currentTimeMillis()));
		System.out.println("now::" + currentTimeToDateString());
	}
}
